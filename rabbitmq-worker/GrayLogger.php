<?php

require_once __DIR__ . '/gelf-php/vendor/autoload.php';

class GrayLogger {

	private $transport;
	private $publisher;
	private $logger;

	public function __construct($host = "127.0.0.1", $port = 12201)
	{
		$this->transport = new Gelf\Transport\UdpTransport($host, $port, Gelf\Transport\UdpTransport::CHUNK_SIZE_LAN);
		$this->publisher = new Gelf\Publisher();
		$this->publisher->addTransport($this->transport);
		$this->logger = new Gelf\Logger($this->publisher, 'NetSuite');
	}

	public function alert($message, $context = array())
	{
		$this->logger->alert($message, $context);
		return $this;
	}

	public function critical($message, $context = array())
	{
		$this->logger->critical($message, $context);
		return $this;
	}

	public function error($message, $context = array())
	{
		$this->logger->error($message, $context);
		return $this;
	}

	public function warning($message, $context = array())
	{
		$this->logger->warning($message, $context);
		return $this;
	}

	public function notice($message, $context = array())
	{
		$this->logger->notice($message, $context);
		return $this;
	}

	public function info($message, $context = array())
	{
		$this->logger->info($message, $context);
		return $this;
	}

	public function debug($message, $context = array())
	{
		$this->logger->debug($message, $context);
		return $this;
	}
}