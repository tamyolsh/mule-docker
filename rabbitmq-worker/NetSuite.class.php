<?php
ini_set('error_reporting', E_ALL & ~E_DEPRECATED & ~E_WARNING & ~E_NOTICE);
ini_set('display_errors', 1);

require_once(__DIR__ . '/NetSuite/NetSuiteService.php');
require_once(__DIR__ . '/GrayLogger.php');

class NetSuite
{
	const SOCKET_TIMEOUT = 180;

	private $logger = null;
	protected $_data;
	protected $_service;

	public function __construct($data)
	{
		$this->_data = $data;
		try
		{
			if (isset($this->_data['gray_logger']) && isset($this->_data['gray_logger']['host']) && isset($this->_data['gray_logger']['port']))
			{
				$this->logger = new GrayLogger($this->_data['gray_logger']['host'], $this->_data['gray_logger']['port']);
			}
		}
		catch (Exception $e)
		{}
		try {
			global $nshost, $nsendpoint;
			global $nsaccount, $nsemail, $nsrole, $nspassword;
			if (isset($data['access']))
			{
				if (isset($data['access']['host']))
				{
					$nshost = $data['access']['host'];
				}
				if (isset($data['access']['endpoint']))
				{
					$nsendpoint = $data['access']['endpoint'];
				}
				if (isset($data['access']['email']))
				{
					$nsemail = $data['access']['email'];
				}
				if (isset($data['access']['pwd']))
				{
					$nspassword = $data['access']['pwd'];
				}
				if (isset($data['access']['role']))
				{
					$nsrole = $data['access']['role'];
				}
				if (isset($data['access']['account']))
				{
					$nsaccount = $data['access']['account'];
				}

			}
		}
		catch (Exception $e) {
			if (isset($this->_data)) {
				Handler::sendErrorEmail($this->_data, $e->getMessage(), $e->getTrace());
			}

			if ($this->logger)
			{
				$this->logger->error('NetSuite Initializing Error', array('exception' => $e));
			}
		}
	}

	protected function _makeCall($action, $arg)
	{
		$_socketTimeout = ini_get('default_socket_timeout');
		ini_set('default_socket_timeout', self::SOCKET_TIMEOUT);
		unset($this->_service);
		$this->_service = new NetSuiteService();

		$response = $this->_service->{$action}($arg);

		ini_set('default_socket_timeout', $_socketTimeout);

		return $response;
	}

	public function processRequest()
	{
		$action = $this->_data['command'];
		$callback = $this->_data['callback'];
		try {
			$arg = $this->formatParameters($this->_data['arg']);		
			$response = $this->_makeCall($action, $arg);
		}
		catch (Exception $e) {
			if (isset($this->_data)) {
				Handler::sendErrorEmail($this->_data, $e->getMessage(), $e->getTrace());
			}

			if ($this->logger)
			{
				$this->logger->error('NetSuite Request Failed', array('exception' => $e, 'request' => $this->_service->client->__getLastRequest(), 'response' => $this->_service->client->__getLastResponse()));
			}
		}
		if ($this->logger)
		{
			$this->logger->debug('NetSuite Request/Response', array('request' => $this->_service->client->__getLastRequest(), 'response' => $this->_service->client->__getLastResponse()));
		}

		return $this->parseResponse($response, $callback, $arg);
	}

	protected function formatParameters($arg = array())
 {
        if (isset($arg['className'])) {
            $obj = new $arg['className']();
            
            foreach ($arg['parameters'] as $key => $param) {
                $obj->{$key} = $this->formatParameters($param);
            }
            
            return $obj;
        } else if (isset($arg['subRequestData'])) {
            $subRequestActions = $arg['subRequestData']['command'];
            $subRequestCallbacks = $arg['subRequestData']['callback'];
            $subRequestArgs = $arg['subRequestData']['arg'];
            
            if (!is_array($subRequestActions)) {
                $subRequestArgs = array($subRequestActions => $subRequestArgs);
                $subRequestCallbacks = array($subRequestActions => $subRequestCallbacks);
                $subRequestActions = array($subRequestActions);
            }
            
            foreach ($subRequestActions as $subRequestAction) {
                if ($subRequestAction == 'default_value') {
                    return $subRequestArgs[$subRequestAction];
                }
                
                try {
                    $subRequestArg = $this->formatParameters($subRequestArgs[$subRequestAction]);
                    $subRequestResponse = $this->_makeCall($subRequestAction, $subRequestArg);
                } catch (Exception $e) {
                    if ($this->logger) {
                        $this->logger->error(
                            'NetSuite Request Failed', 
                            array(
                                'exception' => $e, 
                                'request' => $this->_service->client->__getLastRequest(), 
                                'response' => $this->_service->client->__getLastResponse()
                            )
                        );
                    }
                    break;
                }
                
                if ($this->logger) {
                    $this->logger->debug(
                        'NetSuite Request/Response', 
                        array(
                            'request' => $this->_service->client->__getLastRequest(), 
                            'response' => $this->_service->client->__getLastResponse()
                        )
                    );
                }
                
                $result = $this->parseResponse($subRequestResponse, $subRequestCallbacks[$subRequestAction], $subRequestArgs[$subRequestAction], true);
                
                if ($result !== false) {
                    return $result;
                }
            }
            
            return null;
        } else {
            foreach ($arg as $key => $param) {
                $arg[$key] = $this->formatParameters($param);
            }
            
            return $arg;
        }
 }

	public function parseResponse($response, $callback, $arg = null, $isSubRequest = false)
	{
		switch ($callback) {
			case 'getInternalId':
				if (!$response->readResponse->status->isSuccess) {
					$message = sprintf(
						'%s (%s): %s',
						$response->readResponse->status->statusDetail[0]->code,
						$response->readResponse->status->statusDetail[0]->type,
						$response->readResponse->status->statusDetail[0]->message
					);

					if (isset($this->_data)) {
						if ($isSubRequest) {
							Handler::sendErrorEmail($this->_data, $message, null, true, $arg);
						} else {
							Handler::sendErrorEmail($this->_data, $message);
						}
					}

					if ($this->logger) {
						$this->logger->notice(
							'NetSuite Notice: No Internal Id Found', array('message' => $message));
					}

					return false;
				}
				else {
					return $response->readResponse->record->internalId;
				}
				break;
			case 'getNewInternalId':
				if (!$response->writeResponse->status->isSuccess) {
					$message = sprintf(
						'%s (%s): %s',
						$response->writeResponse->status->statusDetail[0]->code,
						$response->writeResponse->status->statusDetail[0]->type,
						$response->writeResponse->status->statusDetail[0]->message
					);

					if (isset($this->_data)) {
						if ($isSubRequest) {
							Handler::sendErrorEmail($this->_data, $message, null, true, $arg);
						} else {
							Handler::sendErrorEmail($this->_data, $message);
						}
					}

					if ($this->logger) {
						$this->logger->notice('NetSuite Notice: Instance Creation Failed', array('message' => $message));
					}
					return false;
				}
				else {
					return $response->writeResponse->baseRef->internalId;
				}
				break;
			case 'getFirstSelectValueResultInternalId':
				if ($response->getSelectValueResult->status->isSuccess) {
					foreach ($response->getSelectValueResult->baseRefList->baseRef as $result) {
						return $result->internalId;
					}
				} else {
					$message = sprintf(
						'%s (%s): %s',
						$response->getSelectValueResult->status->statusDetail[0]->code,
						$response->getSelectValueResult->status->statusDetail[0]->type,
						$response->getSelectValueResult->status->statusDetail[0]->message
					);

					if (isset($this->_data)) {
						if ($isSubRequest) {
							Handler::sendErrorEmail($this->_data, $message, null, true, $arg);
						} else {
							Handler::sendErrorEmail($this->_data, $message);
						}
					}

					if ($this->logger) {
						$this->logger->notice('NetSuite Notice: Select Request Failed', array('message' => $message));
					}
				}
				return false;
				break;
			case 'getFirstSearchResultInternalId':
				if ($response->searchResult->status->isSuccess) {
					foreach ($response->searchResult->recordList->record as $result) {
						return $result->internalId;
					}
				} else {
					$message = sprintf(
						'%s (%s): %s',
						$response->searchResult->status->statusDetail[0]->code,
						$response->searchResult->status->statusDetail[0]->type,
						$response->searchResult->status->statusDetail[0]->message
					);

					if (isset($this->_data)) {
						if ($isSubRequest) {
							Handler::sendErrorEmail($this->_data, $message, null, true, $arg);
						} else {
							Handler::sendErrorEmail($this->_data, $message);
						}
					}

					if ($this->logger) {
						$this->logger->notice('NetSuite Notice: Search Request Failed', array('message' => $message));
					}
				}
				return false;
				break;
			case 'getInitializedObject':
				if ($response->readResponse->status->isSuccess) {
					return $response->readResponse->record;
				} else {
					$message = sprintf(
						'%s (%s): %s',
						$response->readResponse->status->statusDetail[0]->code,
						$response->readResponse->status->statusDetail[0]->type,
						$response->readResponse->status->statusDetail[0]->message
					);

					if (isset($this->_data)) {
						if ($isSubRequest) {
							Handler::sendErrorEmail($this->_data, $message, null, true, $arg);
						} else {
							Handler::sendErrorEmail($this->_data, $message);
						}
					}

					if ($this->logger) {
						$this->logger->notice('NetSuite Notice: Read Request Failed', array('message' => $message));
					}
				}
				return false;
				break;
			case 'returnGetResultAsArray':
				$result = array();

				if ($response->readResponse->status->isSuccess) {
					$result = $this->asArray((array)$response->readResponse->record);
				} else {
					$message = sprintf(
						'%s (%s): %s',
						$response->readResponse->status->statusDetail[0]->code,
						$response->readResponse->status->statusDetail[0]->type,
						$response->readResponse->status->statusDetail[0]->message
					);

					if (isset($this->_data)) {
						if ($isSubRequest) {
							Handler::sendErrorEmail($this->_data, $message, null, true, $arg);
						} else {
							Handler::sendErrorEmail($this->_data, $message);
						}
					}

					if ($this->logger) {
						$this->logger->notice('NetSuite Notice: Read Request Failed', array('message' => $message));
					}
				}
				return $result;
				break;
			default:
				if (!$response->writeResponse->status->isSuccess) {
					$message = sprintf(
						'%s (%s): %s',
						$response->writeResponse->status->statusDetail[0]->code,
						$response->writeResponse->status->statusDetail[0]->type,
						$response->writeResponse->status->statusDetail[0]->message
					);

					if (isset($this->_data)) {
						if ($isSubRequest) {
							Handler::sendErrorEmail($this->_data, $message, null, true, $arg);
						} else {
							Handler::sendErrorEmail($this->_data, $message);
						}
					}

					$this->_processCreationFailure($arg, $response);
				} else {
					$this->_processCreationSuccess($arg);
				}
				return $response;
				break;
		}
	}

	private function asArray($array)
	{
		foreach ($array as $key => $element) {
			if (is_object($element) || is_array($element)) {
				$array[$key] = $this->asArray((array)$element);
			}
		}
		return $array;
	}

	private function updateOrder($key, $url, $orderId, $status) {
		$data = array(
			'key'      => $key,
			'order_id' => $orderId,
			'status'   => $status
		);
		$fieldsString = http_build_query($data);
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($data));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fieldsString);
		curl_exec($ch);
		curl_close($ch);
	}

	private function _processCreationSuccess($arg)
	{
		if (isset($this->_data['key']) && isset($this->_data['url']) && isset($arg->record->externalId)) {
			try {
				if ($arg->record instanceof CashSale) {
					$status = 1;
				} elseif ($arg->record instanceof CashRefund) {
					$status = 2;
				} else {
					$status = 0;
				}

				if ($status) {
					$this->updateOrder(
						$this->_data['key'],
						$this->_data['url'],
						substr($arg->record->externalId, 2),
						$status
					);
				}
			} catch (Exception $e) {

			}
		}
	}

	private function _processCreationFailure($arg, $response)
	{
		if (isset($this->_data['key']) && isset($this->_data['url']) && isset($arg->record->externalId)) {
			try {
				$errorCode = $response->writeResponse->status->statusDetail[0]->code;
				$isDuplicate = ($errorCode == 'DUP_RCRD');

				if ($isDuplicate && $arg->record instanceof CashSale) {
					$status = 1;
				} elseif ($isDuplicate && $arg->record instanceof CashRefund) {
					$status = 2;
				} else {
					$status = 0;
				}

				if ($status) {
					$this->updateOrder(
						$this->_data['key'],
						$this->_data['url'],
						substr($arg->record->externalId, 2),
						$status
					);
				}
			} catch (Exception $e) {

			}
		}
	}
}
