<?php
ini_set('error_reporting', E_ALL & ~E_DEPRECATED & ~E_WARNING & ~E_NOTICE);

require_once(__DIR__ . '/NetSuite.class.php');
require_once(__DIR__ . '/worker_config.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;

$connection = new AMQPStreamConnection(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER, RABBITMQ_PASS);
$channel = $connection->channel();
$channel->queue_declare(RABBITMQ_NT_QUEUE, false, true, false, false);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

$callback = function($req) {

    $message = $req->body;
    if ($req->delivery_info['routing_key'] == 'error') {
        $result = $message;
    }
    $json_message = json_decode($message, true);
    $instance = Handler::getInstance();
    $result = $instance->processNetsuiteRequest($json_message);
    
    $msg = new PhpAmqpLib\Message\AMQPMessage(
            $result, array('reply_to' => RABBITMQ_NT_QUEUE)
    );

    $req->delivery_info['channel']->basic_publish(
            $msg, '', $req->get('reply_to'));
};
$channel->basic_qos(null, 1, null);
$channel->basic_consume(RABBITMQ_NT_QUEUE, '', false, true, false, false, $callback);

while (count($channel->callbacks)) {
    $channel->wait();
}
$channel->close();
$connection->close();

class Handler
{
	const MAIL_FROM = 'gearman-netsuite@twinamerica.com';

	private static $_instance = null;

        public static function getInstance() {
		if (is_null(self::$_instance)) {
	    	self::$_instance = new self();
	}

		return self::$_instance;
    	}

	public static function sendErrorEmail($request, $errorMessage, $trace = null, $isSubRequest = false, $subRequest = null)
	{
		if (!isset($request['error_email'])) {
			return;
		}

		try {
			$requestEntity = $requestEntityId = $requestAction = '';
			$subRequestEntity = $subRequestAction = '';

			$request = json_decode(json_encode($request), true);
			$subRequest = $isSubRequest ? json_decode(json_encode($subRequest), true) : array();

			if (isset($request['baseRef']['type'])) {
				$requestEntity = ucwords($request['baseRef']['type']);
				$requestEntityId = $request['baseRef']['externalId'];
				$requestAction = 'Search';
			} elseif (isset($request['arg']['parameters']['record'])) {
				$requestRecord = $request['arg']['parameters']['record'];
				$requestEntity = ucwords($requestRecord['className']);
				$requestEntityId = $requestRecord['parameters']['externalId'];
				$requestAction = $request['command'] == 'add' ? 'Creation' : ($request['command'] == 'update' ? 'Update' : '');
			}

			if ($isSubRequest) {
				if (isset($subRequest['parameters']['baseRef']['parameters']['type'])) {
					$subRequestEntity = ucwords($subRequest['parameters']['baseRef']['parameters']['type']);
					$subRequestAction = 'Search';
				} elseif (isset($subRequest['parameters']['record']['className'])) {
					$subRequestEntity = ucwords($subRequest['parameters']['record']['className']);
					$subRequestAction = 'Creation';
				}

				// We shouldn't send error email if customer is not found
				if ($subRequestEntity == 'Customer' && $subRequestAction == 'Search') {
					return;
				}

				$emailSubject = sprintf(
					'Netsuite Error: %s %s Failed (%s %s %s)',
					$subRequestEntity,
					$subRequestAction,
					$requestEntity,
					$requestEntityId,
					$requestAction
				);
			} else {
				$emailSubject = sprintf(
					'Netsuite Error: %s %s %s Failed',
					$requestEntity,
					$requestEntityId,
					$requestAction
				);
			}

			$emailHTML = "<style>td{padding:10px;}pre{overflow:auto;height:250px;font-size:10px;}</style><table>";

			$emailHTML .= sprintf(
				"<tr><td><b>Entity</b></td><td>%s %s</td></tr>",
				$isSubRequest ? $subRequestEntity : $requestEntity,
				$isSubRequest ? '' : $requestEntityId
			);

			$emailHTML .= sprintf(
				"<tr><td><b>Action</b></td><td>%s</td></tr>",
				$isSubRequest ? $subRequestAction : $requestAction
			);

			$emailHTML .= sprintf("<tr><td><b>Error</b></td><td>%s</td></tr>", $errorMessage);

			if ($trace) {
				$emailHTML .= sprintf("<tr><td><b>Trace</b></td><td><pre>%s</pre></td></tr>", print_r($trace, true));
			}

			$emailHTML .= sprintf(
				"<tr><td><b>%s</b></td><td><pre>%s</pre></td></tr>",
				$isSubRequest ? 'SubRequest' : 'Request',
				print_r($isSubRequest ? $subRequest : $request, true)
			);

			$emailHTML .= "</table>";
			$emailHeaders = "From:" . self::MAIL_FROM . "\r\n";
			$emailHeaders .= "MIME-Version: 1.0" . "\r\n";
			$emailHeaders .= "Content-type:text/html;charset=UTF-8" . "\r\n";

			$emails = explode(',', $request['error_email']);

			foreach ($emails as $to) {
				$to = trim($to);

				if ($to) {
					mail($to, $emailSubject, $emailHTML, $emailHeaders);
				}
			}
		} catch (Exception $e) {

		}
	}

	public function processNetsuiteRequest(array $data)
	{
		try
		{		
			$netsuite = new NetSuite($data);
			$result = $netsuite->processRequest();
			//$job->sendComplete(serialize($result));
			$resultArr = get_object_vars($result);
			$resultXML = $this->generateValidXmlFromArray($resultArr);
			print_r($resultXML);
			return $resultXML;
		}
		catch (Exception $e)
		{
			if (isset($data)) {
				//GearmanJobWorker::sendErrorEmail($data, $e->getMessage(), $e);
				$this->sendErrorEmail($data, $e->getMessage(), $e);
			}

			//$job->sendException($e->getMessage());
			//$job->sendFail();
			echo $e->getMessage();
		}
	}

	private function generateValidXmlFromArray($array, $node_block='nodes', $node_name='node') {
		$xml = '<?xml version="1.0" encoding="UTF-8" ?>';
		$xml .= '<' . $node_block . '>';
		$xml .= self::generateXmlFromArray($array, $node_name);
		$xml .= '</' . $node_block . '>';
		return $xml;
   	}

        private function generateXmlFromArray($array, $node_name) {
		$xml = '';

		if (is_array($array) || is_object($array)) {
		    foreach ($array as $key=>$value) {
			if (is_numeric($key)) {
			    $key = $node_name;
			}

			$xml .= '<' . $key . '>' . self::generateXmlFromArray($value, $node_name) . '</' . $key . '>';
		    }
		} else {
		    $xml = htmlspecialchars($array, ENT_QUOTES);
		}
		return $xml;
        }

}
