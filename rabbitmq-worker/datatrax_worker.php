<?php

require_once __DIR__ . '/gelf-php/vendor/autoload.php';
require_once(__DIR__ . '/worker_config.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;

$connection = new AMQPStreamConnection(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER, RABBITMQ_PASS);
$channel = $connection->channel();


$channel->queue_declare(RABBITMQ_DT_QUEUE, false, true, false, false);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";


$callback = function($req) {


    $message = $req->body;
   if ($req->delivery_info['routing_key'] == 'error') {

        $result = $message;

    }
    $json_m = json_decode($message, true);
    $json_message = $json_m['info'];
    $acceptHeader = $json_m['params']['header'];
    $instance = Handler::getInstance();
    if ($json_message['request']['request_data']['command'] == 'CancelVoucher') {
        echo 'CancelVoucher';
        $result = $instance->cancelVoucherHandle($json_message, $acceptHeader);
    } else if ($json_message['request']['request_data']['command'] == 'IssueVoucher') {
        echo 'IssueVoucher';
        $result = $instance->issueVoucherHandle($json_message, $acceptHeader);
    } else if ($json_message['request']['request_data']['command'] == 'ReissueVoucher') {
        echo 'ReissueVoucher';
        $result = $instance->issueVoucherHandle($json_message, $acceptHeader);
    } else {
        echo 'GetProductCatalog';
        $result = $instance->catalogHandle($json_message, $acceptHeader);
    }


    $msg = new PhpAmqpLib\Message\AMQPMessage(
            $result, array('reply_to' => RABBITMQ_DT_QUEUE)
    );

    $req->delivery_info['channel']->basic_publish(
            $msg, '', $req->get('reply_to'));
};
$channel->basic_qos(null, 1, null);
$channel->basic_consume(RABBITMQ_DT_QUEUE, '', false, true, false, false, $callback);
while (count($channel->callbacks)) {

    $channel->wait();
}
$channel->close();
$connection->close();

class Handler {

    private static $_instance = null;
    private $notifier;

    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Send IssueVoucher/ReissueVoucher requests to Datatrax
     *
     * @param GearmanJob $job
     */
    public function issueVoucherHandle(array $job, $header) {
        return $this->_handle($job, true, $header);
    }

    /**
     * Send CancelVoucher request to Datatrax
     *
     * @param GearmanJob $job
     */
    public function cancelVoucherHandle(array $job, $header) {
        return $this->_handle($job, true, $header);
    }

    /**
     * Send GetCatalog request to Datatrax
     *
     * @param GearmanJob $job
     */
    public function catalogHandle(array $job, $header) {
        return $this->_handle($job, false, $header);
    }

    /**
     * Send request to Datatrax
     *
     * @param GearmanJob $job
     * @param bool|false $isSequenceNumberNeeded
     */
    protected function _handle(array $data, $isSequenceNumberNeeded = false, $header) {
        //$data = unserialize($job->workload());
        // $data=$job;
        $this->notifier = new Notifier($data);

        try {
            if ($isSequenceNumberNeeded) {
                $sequenceNumber = $this->_getSequenceNumber($data);
                $data['request']['command_data']['header']['sequence_number'] = $sequenceNumber;
            }

            $requestXML = $this->_prepareXML($data['request']['request_data'], $data['request']['command_data']);
            //var_dump($requestXML);
            $responseXML = $this->_sendRequest($data['datatrax']['host'], $data['datatrax']['port'], $requestXML);

            if ((int) $responseXML->ErrorStatus !== 0) {
                //echo 'if ((int)$responseXML->ErrorStatus !== 0) {';
                $this->_sendMail((string) $responseXML->ErrorMessage, $requestXML);
            }
        } catch (Exception $e) {
            $this->_sendMail($e->getMessage(), $data['request']);
            // echo 'catch (Exception $e) {';
            $responseXML = $this->_responseFromException($e->getMessage(), $data['request']['request_data']['command']);
            //var_dump($responseXML);
        }

        if (isset($data['request']['back_url'])) {
     	        $response = $this->_sendResponse($data['request']['back_url'], $responseXML, $header, $data['request']['request_data']);           
 	}
	else {
		$response = $this->_sendResponse($data['request']['back_url'], $responseXML, $header, $data['request']['request_data']);
	}
        print($response);
        return $response;

        //$job->sendComplete($responseXML->asXML());
    }

    /**
     * Do curl request to Datatrax
     *
     * @param $host
     * @param $port
     * @param $request
     * @return SimpleXMLElement|string
     * @throws Exception
     */
    protected function _sendRequest($host, $port, $request) {
        $this->_log('Request: ' . $request);
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $host);
        curl_setopt($curl, CURLOPT_PORT, $port);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 15);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        $result = curl_exec($curl);
        //var_dump($result);
        if ($result === false) {
            $curlError = curl_error($curl);
            $curlErrno = curl_errno($curl);
            curl_close($curl);

            if ($curlErrno == 28) {
                $this->_log('Response: Connection timed out');
                throw new Exception('Connection timed out');
            } else {
                $this->_log('Response: cURL: ' . $curlError . ' (' . $curlErrno . ')');
                throw new Exception('cURL: ' . $curlError . ' (' . $curlErrno . ')');
            }
        } else {
            $this->_log('Response: ' . $result);
        }

        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	$totalTime = curl_getinfo($curl, CURLINFO_TOTAL_TIME);
	$redirectTime = curl_getinfo($curl, CURLINFO_REDIRECT_TIME);
	$redirectUrl = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);

        //var_dump($httpCode);
        if ($httpCode != '200') {
            curl_close($curl);
            throw new Exception('cURL: Response http code: ' . $httpCode);
        }

        curl_close($curl);

        if (strpos($result, '<?xml') !== false) {
	    $resultXML = simplexml_load_string($result);
            $transactionXML = $resultXML->addChild('TransactionData');
            $transactionXML->addChild('TotalTime', $totalTime*1000);
            $transactionXML->addChild('RedirectTime', $redirectTime);
	    $transactionXML->addChild('RedirectUrl', $redirectUrl);

	    return $resultXML;
        } else {
            throw new Exception('Response is not a valid XML string');
        }
    }

    /**
     * Send response to specific "back url".
     * Used for background jobs.
     *
     * @param $host
     * @param SimpleXMLElement $responseXML
     */
    protected function _sendResponse($host, SimpleXMLElement $responseXML, $header, $requestData) {

	//Added details about orginal request to response.
	$requestXML = $responseXML->addChild('OrginalRequest');
	$requestXML->addChild('Version', $requestData['version']);
        $requestXML->addChild('TerminalKey', $requestData['terminal_key']);
        $requestXML->addChild('TimeStamp', $requestData['time_stamp']);
        $requestXML->addChild('Command', $requestData['command']);

	if (strpos($header, 'json') !== false) {
		$responseFormat = json_encode($responseXML);
	}
	else {
		$responseFormat = $responseXML->asXML();
	}
        try {
            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $host);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: multipart/*'));
            curl_setopt($curl, CURLOPT_POSTFIELDS, array('response' => $responseFormat));
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_TIMEOUT, 30);

           // var_dump($responseXML->asXML());
            $result=curl_exec($curl);
            var_dump($result);
            curl_close($curl);
	    return $responseFormat;
        } catch (Exception $e) {
            
        }
    }

    /**
     * Send GetSequenceNumber request to Datatrax
     *
     * @param $data
     * @return string
     * @throws Exception
     */
    protected function _getSequenceNumber($data) {
        $request['request_data'] = $data['request']['request_data'];
        $request['request_data']['command'] = 'GetSequenceNumber';

        $requestXML = $this->_prepareXML($request['request_data']);
        $responseXML = $this->_sendRequest($data['datatrax']['host'], $data['datatrax']['port'], $requestXML);

        if ((int) $responseXML->ErrorStatus === 0) {
            $sequenceNumber = (string) $responseXML->CommandResponse->sequence_number;

            if (empty($sequenceNumber)) {
                throw new Exception('Sequence number is empty');
            } else {
                return $sequenceNumber;
            }
        } else {
            throw new Exception((string) $responseXML->ErrorMessage);
        }
    }

    /**
     * Prepare xml request from array
     *
     * @param array $requestData
     * @param array $commandData
     * @return mixed
     */
    protected function _prepareXML($requestData, $commandData = array()) {
        $requestXML = new SimpleXMLElement('<Request/>');

        $requestXML->addChild('Version', $requestData['version']);
        $requestXML->addChild('TerminalKey', $requestData['terminal_key']);
        $requestXML->addChild('TerminalPassword', $requestData['terminal_password']);
        $requestXML->addChild('TimeStamp', $requestData['time_stamp']);
        $requestXML->addChild('Command', $requestData['command']);

        if ($commandData) {
            $commandDataXML = $requestXML->addChild('CommandData');
            $headerXML = $commandDataXML->addChild('Header');

            $headerXML->addChild('sequence_number', $commandData['header']['sequence_number']);
            $headerXML->addChild('transaction_date', $commandData['header']['transaction_date']);
            $headerXML->addChild('transaction_time', $commandData['header']['transaction_time']);
            $headerXML->addChild('voucher_number', $commandData['header']['voucher_number']);

            if (isset($commandData['products'])) {
                $productsXML = $commandDataXML->addChild('Products');

                foreach ($commandData['products'] as $product) {
                    $productXML = $productsXML->addChild('Product');

                    $productXML->addChild('product_id', $product['product_id']);
                    $productXML->addChild('sub_product_id', $product['sub_product_id']);
                    $productXML->addChild('product_version', $product['product_version']);
                    $productXML->addChild('price', $product['price']);
                    $productXML->addChild('quantity', $product['quantity']);
                    $productXML->addChild('discount_id', $product['discount_id']);
                    $productXML->addChild('discount_rate', $product['discount_rate']);
                    $productXML->addChild('discount_amount', $product['discount_amount']);
                    $productXML->addChild('service_charge', $product['service_charge']);

                    $ticketParametersXML = $productXML->addChild('TicketParameters');

                    foreach ($product['ticket_parameters'] as $ticket) {
                        $ticketParameterXML = $ticketParametersXML->addChild('TicketParameter');

                        $ticketParameterXML->addChild('package_product_id', $ticket['package_product_id']);
                        $ticketParameterXML->addChild('travel_date', $ticket['travel_date']);
                        $ticketParameterXML->addChild('travel_time', $ticket['travel_time']);
                        $ticketParameterXML->addChild('origin_id', $ticket['origin_id']);
                        $ticketParameterXML->addChild('destination_id', $ticket['destination_id']);
                    }
                }
            }

            if (isset($commandData['payment'])) {
                $payment = $commandData['payment'];

                $paymentsXML = $commandDataXML->addChild('Payments');
                $paymentXML = $paymentsXML->addChild('Payment');

                $paymentXML->addChild('amount', $payment['amount']);
                $paymentXML->addChild('account_number', $payment['account_number']);
                $paymentXML->addChild('cvv2', $payment['cvv2']);
                $paymentXML->addChild('expiry_date', $payment['expiry_date']);
                $paymentXML->addChild('avs_address', $payment['avs_address']);
                $paymentXML->addChild('avs_zip', $payment['avs_zip']);
                $paymentXML->addChild('authorization_number', $payment['authorization_number']);
                $paymentXML->addChild('reference_number', $payment['reference_number']);
                $paymentXML->addChild('trace_number', $payment['trace_number']);
            }
        }
        
        return $requestXML->asXML();
    }

    /**
     * Prepare xml response with exception message
     *
     * @param $message
     * @param $command
     * @return SimpleXMLElement
     */
    protected function _responseFromException($message, $command) {
        $responseXML = new SimpleXMLElement('<Response/>');
        $responseXML->addChild('Command', $command);
        $responseXML->addChild('ErrorStatus', 1);
        $responseXML->addChild('ErrorMessage', $message);
        return $responseXML;
    }

    /**
     * Send notification email with error/exception message
     *
     * @param $message
     * @param null $request
     */
    protected function _sendMail($message, $request = null) {
        $message = 'Datatrax RabbitMQ Worker: ' . $message;
        $this->notifier->sendMail($message, $request);
    }

    /**
     * Log requests/responses
     *
     * @param $message
     */
    protected function _log($message) {
        try {
            if ($this->notifier) {
                $this->notifier->log($message);
            }
        } catch (Exception $e) {
            
        }
    }

}

class Notifier {

    const MAIL_FROM = 'datatrax@twinamerica.com';
    const MAIL_SUBJECT = 'DataTrax Worker Error';
    const GRAYLOG_FACILITY = 'Datatrax';

    protected $email;
    protected $graylogger;

    function __construct($data) {
        if (isset($data['notification_email'])) {
            $this->email = $data['notification_email'];
        }

        if (isset($data['gray_logger']['host']) && isset($data['gray_logger']['port'])) {
            try {
                $transport = new Gelf\Transport\UdpTransport(
                        $data['gray_logger']['host'], $data['gray_logger']['port'], Gelf\Transport\UdpTransport::CHUNK_SIZE_LAN
                );

                $publisher = new Gelf\Publisher($transport);
                $this->graylogger = new Gelf\Logger($publisher, self::GRAYLOG_FACILITY);
            } catch (Exception $e) {
                
            }
        }
    }

    public function sendMail($message, $request = null) {
        if ($this->email) {
            $subject = self::MAIL_SUBJECT;

            if ($request instanceof SimpleXMLElement) {
                $subject .= sprintf(': %s %s Failed', $request->Command, $request->CommandData->Header->voucher_number
                );

                $message .= "\r\n" . 'Request: ' . "\r\n" . $request->asXML();
            } elseif (is_array($request)) {
                $subject .= sprintf(': %s %s Failed', $request['request_data']['command'], $request['command_data']['header']['voucher_number']
                );

                $message .= "\r\n" . 'Request: ' . "\r\n" . print_r($request, true);
            }

            $headers = "From:" . self::MAIL_FROM . "\r\n";
            $headers .= "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            mail($this->email, $subject, '<pre>' . $message . '</pre>', $headers);
        }
    }

    public function log($message) {
        if ($this->graylogger) {
            $this->graylogger->info($message);
        }
    }

}
