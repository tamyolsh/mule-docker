FROM ubuntu:latest

# Install prerequisites
RUN apt-get update \
    && apt-get install -y software-properties-common \
                          python \
                          curl \
			  python-software-properties \
			  locales \
                          sendmail

# Install php5
RUN locale-gen en_US.UTF-8
ENV LANG=en_US.UTF-8
RUN add-apt-repository ppa:ondrej/php \
    && apt-get update \
    && apt-get install -y php5.6 \
                          php5.6-mbstring \
                          php5.6-bcmath \
                          php5.6-xml \
                          php5.6-curl

# Install java8
RUN add-apt-repository -y ppa:webupd8team/java \
    && apt-get update \
    && echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections \
    && apt-get install -y oracle-java8-installer

# Install and config supervisor (for running proccess background)
RUN apt-get install -y supervisor
ADD systems.conf /etc/supervisor/conf.d/systems.conf
RUN chmod +x /etc/supervisor/conf.d/systems.conf

#Install Mule
RUN cd ~ && \
    wget https://repository-master.mulesoft.org/nexus/content/repositories/releases/org/mule/distributions/mule-standalone/3.8.0/mule-standalone-3.8.0.tar.gz && \
    echo "d9279b3f0373587715613341a16483f3 mule-standalone-3.8.0.tar.gz" | md5sum -c

RUN cd /opt && \
    tar xvzf ~/mule-standalone-3.8.0.tar.gz && \
    rm ~/mule-standalone-3.8.0.tar.gz && \
    ln -s /opt/mule-standalone-3.8.0 /opt/mule

# Define environment variables.
ENV MULE_HOME /opt/mule

# Copy mule project to apps
COPY hub.zip /opt/mule/apps

# Define mount points.
VOLUME ["/opt/mule/logs", "/opt/mule/apps"]

# Define working directory.
WORKDIR /opt/mule

#EXPOSE 80

ENTRYPOINT /usr/bin/supervisord -c /etc/supervisor/supervisord.conf && \
           /opt/mule/bin/mule

