#!/bin/bash

COMMAND='/usr/local/bin/rabbitmqadmin'

$COMMAND declare queue name=hub durable=true
$COMMAND declare queue name=hub2 durable=true
$COMMAND declare queue name=hub3 durable=true
$COMMAND declare queue name=pass durable=true
$COMMAND declare queue name=netsuite durable=true
$COMMAND declare queue name=datatraxstream durable=true
$COMMAND declare queue name=datatrax durable=true
$COMMAND declare queue name=actourex durable=true

$COMMAND declare binding source="amq.direct" destination="datatrax" routing_key="CancelVoucher"
$COMMAND declare binding source="amq.direct" destination="datatrax" routing_key="GetProductCatalog"
$COMMAND declare binding source="amq.direct" destination="datatrax" routing_key="IssueVoucher"
$COMMAND declare binding source="amq.direct" destination="datatrax" routing_key="ReissueVoucher"
$COMMAND declare binding source="amq.direct" destination="datatrax" routing_key="error"

rabbitmqctl add_user mule mule
rabbitmqctl set_user_tags mule administrator
rabbitmqctl set_permissions -p / mule ".*" ".*" ".*"
